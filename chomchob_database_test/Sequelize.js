const Product = sequelize.define("product", {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  name: Sequelize.STRING,
  description: Sequelize.STRING,
  price: Sequelize.INTEGER,
  promotion_price: Sequelize.INTEGER,
  promotion_date: Sequelize.STRING,
  code: Sequelize.STRING,
  sale_date: {
    Type: Sequelize.DATEONLY,
    timestamps: true,
  },
  end_date: {
    Type: Sequelize.DATEONLY,
    timestamps: false,
  },
});

const Order = sequelize.define("order", {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  product_name: Sequelize.STRING,
  amount: Sequelize.INTEGER,
  total: Sequelize.INTEGER,
  date: {
    Type: Sequelize.DATE,
    timestamps: true,
  }
});


