const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
const express = require("express");

const adminRoute = require("./admin");
const userRoute = require("./user");

dotenv.config();

const app = express();

app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/api/admin", adminRoute);
app.use("/api/user", userRoute);

app.all("*", (req, res) => {
  res.sendStatus(404);
});

app.listen(process.env.PORT, process.env.HOST, () => {
  console.log(
    `🔥🔥🔥 SERVER RUNNING 🚀🚀 http://${process.env.HOST}:${process.env.PORT}`
  );
});
