const express = require("express");
var pool = require("./database");

const router = express.Router();

router.post("/", async (req, res) => {
    inpCyptoName = req.body.crypto_name;
    inpRateToBath = req.body.rate_to_bath;
    inpRateToCrpto = req.body.rate_to_crypto;

    if (!inpCyptoName) {
      res.status(400);
      res.send({ status: "fail", message: "Json data is invalid" });
    } else {
        const [rows, fields] = await pool.query(
          "SELECT * FROM `crypto` WHERE `crypto_name` = ? LIMIT 1;",
          [inpCyptoName]
        );

        if (rows.length >= 1) {
          res.status(400);
          res.send({
            status: "fail",
            message: "cryptocurrency already exists",
          });
        } else {
          await pool.execute(
            "INSERT INTO `crypto` (`crypto_name`, `rate_to_bath`, `rate_to_crypto`) VALUE (?,?,?);",
            [inpCyptoName, inpRateToBath, inpRateToCrpto]
          );
          res.sendStatus(201);
        }
    }
});

router.put("/edit/rate/:id", async (req, res) => {
  inpParamsId = req.params.id;
  inpRateToBath = req.body.rate_to_bath;
  inpRateToCrpto = req.body.rate_to_crypto;
  inpTel = req.body.tel;
  await pool.execute(
    "UPDATE `crypto` SET `rate_to_bath` = ?, `rate_to_crypto` = ? WHERE `crypto_id` = ?;",
    [inpRateToBath, inpRateToCrpto, inpParamsId]
  );
  res.sendStatus(204);
});

router.get("/", async (req, res) => {
  const [rows, fields] = await pool.query(
    "SELECT DISTINCT `crypto_name` FROM `amount`;"
    );
    var data = []
    for (let i = 0; i < rows.length; i++) {
      const [rows2, fields] = await pool.query(
        "SELECT SUM(`total`) AS `total`, `crypto_name` FROM `amount` WHERE `crypto_name` = ?;",
        [rows[i].crypto_name]
      );
        data.push(rows2);
    }
  res.send(data);
});

router.put("/edit/total/:id", async (req, res) => {
  inpParamsId = req.params.id;
  inpCyptoName = req.body.crypto_name;
  inpTotal = req.body.total;
  await pool.execute(
    "UPDATE `amount` SET `total` = ? WHERE `user_id` = ? AND `crypto_name` = ?;",
    [inpTotal, inpParamsId, inpCyptoName]
  );
    res.sendStatus(204);
});


module.exports = router;