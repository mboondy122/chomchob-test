/*
 Navicat Premium Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MariaDB
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : chomchob-test

 Target Server Type    : MariaDB
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 02/12/2022 17:50:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for amount
-- ----------------------------
DROP TABLE IF EXISTS `amount`;
CREATE TABLE `amount`  (
  `amount_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `crypto_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `total` decimal(9, 4) NULL DEFAULT NULL,
  PRIMARY KEY (`amount_id`) USING BTREE,
  INDEX `crypto_name`(`crypto_name`, `total`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of amount
-- ----------------------------
INSERT INTO `amount` VALUES (1, 1, 'XRP', 10.0000);
INSERT INTO `amount` VALUES (2, 2, 'XRP', 20.0000);
INSERT INTO `amount` VALUES (3, 3, 'ADA', 16.3399);
INSERT INTO `amount` VALUES (4, 3, 'DOGE', 20.0000);
INSERT INTO `amount` VALUES (6, 3, 'XRP', 5.0000);

-- ----------------------------
-- Table structure for crypto
-- ----------------------------
DROP TABLE IF EXISTS `crypto`;
CREATE TABLE `crypto`  (
  `crypto_id` int(11) NOT NULL AUTO_INCREMENT,
  `crypto_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `rate_to_bath` decimal(9, 4) NULL DEFAULT NULL,
  `rate_to_crypto` decimal(9, 4) NULL DEFAULT NULL,
  PRIMARY KEY (`crypto_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of crypto
-- ----------------------------
INSERT INTO `crypto` VALUES (1, 'XRP', 13.9800, 0.0715);
INSERT INTO `crypto` VALUES (2, 'USDC', 34.8900, 0.0286);
INSERT INTO `crypto` VALUES (3, 'ADA', 11.0300, 0.0907);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `fk_user_01`(`amount`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'user01', 1);
INSERT INTO `user` VALUES (2, 'user02', 2);
INSERT INTO `user` VALUES (3, 'user03', 3);

SET FOREIGN_KEY_CHECKS = 1;
