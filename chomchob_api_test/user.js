const express = require("express");
var pool = require("./database");

const router = express.Router();

router.put("/same/:id", async (req, res) => {
  inpParamsId = req.params.id;
  inpCryptoName = req.body.crypto_name;
  inpTotal = req.body.total;
  inpToOther = req.body.user_id;

  const [rows, fields] = await pool.query(
    "SELECT `total` FROM `amount` WHERE `crypto_name` = ? AND `user_id` = ? LIMIT 1;",
    [inpCryptoName, inpParamsId]
  );

  balance_1 = parseInt(rows[0].total) - parseInt(inpTotal);

  await pool.execute(
    "UPDATE `amount` SET `total` = ? WHERE `user_id` = ? AND `crypto_name` = ?;",
    [balance_1, inpParamsId, inpCryptoName]
  );

  const [rows2, fields2] = await pool.query(
    "SELECT `total` FROM `amount` WHERE `crypto_name` = ? AND `user_id` = ? LIMIT 1;",
    [inpCryptoName, inpToOther]
  );

  balance_2 = parseInt(rows2[0].total) + parseInt(inpTotal);

  await pool.execute(
    "UPDATE `amount` SET `total` = ? WHERE `user_id` = ? AND `crypto_name` = ?;",
    [balance_2, inpToOther, inpCryptoName]
  );

  res.status(200).send({ msg: "transfer successfuly" });
});

router.put("/diff/:id", async (req, res) => {
  inpParamsId = req.params.id;
  inpCryptoName = req.body.crypto_name;
  inpTotal = req.body.total;
  inpDiffCrypto = req.body.diff_crypto_name;
  inpToOther = req.body.user_id;

  const [rows, fields] = await pool.query(
    "SELECT `total` FROM `amount` WHERE `crypto_name` = ? AND `user_id` = ? LIMIT 1;",
    [inpCryptoName, inpParamsId]
  );

  balance_1 = rows[0].total - parseInt(inpTotal);

  await pool.execute(
    "UPDATE `amount` SET `total` = ? WHERE `user_id` = ? AND `crypto_name` = ?;",
    [balance_1, inpParamsId, inpCryptoName]
  );

  const [rows2, fields2] = await pool.query(
    "SELECT `rate_to_bath`, `rate_to_crypto` FROM `crypto` WHERE `crypto_name` = ? LIMIT 1;",
    [inpCryptoName]
  );

  convert_to_bath = parseInt(inpTotal) * rows2[0].rate_to_bath;

  const [rows3, fields3] = await pool.query(
    "SELECT `rate_to_bath`, `rate_to_crypto` FROM `crypto` WHERE `crypto_name` = ? LIMIT 1;",
    [inpDiffCrypto, inpToOther]
  );

  const [rows4, fields4] = await pool.query(
    "SELECT `total` FROM `amount` WHERE `crypto_name` = ? AND `user_id` = ? LIMIT 1;",
    [inpDiffCrypto, inpToOther]
  );

  convert_to_crypto = convert_to_bath * rows3[0].rate_to_crypto;
  balance_2 = parseFloat(rows4[0].total) + parseFloat(convert_to_crypto);

  await pool.execute(
    "UPDATE `amount` SET `total` = ? WHERE `user_id` = ? AND `crypto_name` = ?;",
    [balance_2, inpToOther, inpDiffCrypto]
  );

  res.status(200).send({ msg: "transfer successfuly" });
});

module.exports = router;
