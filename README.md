
## Chomchob Backend Testing

**ส่วนที่ 1  Programming part**
    
จะอยู่ใน folder : chomchob_api_test โดยจะมีไฟล์ database.sql ที่เป็น database ของ api นี้

- API Admin increase and decrease user cryptocurrency balance

    METHOD : PUT

    Path : http://localhost:3000/api/admin/edit/total/{id_user}

    parameter : id_user ที่ต้องการเปลี่ยน

    JSON REQ

    ```JSON
    {
        "crypto_name": "",  //  ชื่อ cryptocurrency ที่ต้องการเปลี่ยน
        "total": ""         //  จำนวน cryptocurrency ที่เปลี่ยนไป
    }
    ```

    RES status 204
    #

- API Admin see all total balance of all cryptocurrency.

    METHOD : GET

    Path : http://localhost:3000/api/admin

    JSON RES status 200

    ```JSON
    [
        [
            {
                "total": "16.3399",
                "crypto_name": "ADA"
            }
        ],
        [
            {
                "total": "20.0000",
                "crypto_name": "DOGE"
            }
        ],
        [
            {
                "total": "35.0000",
                "crypto_name": "XRP"
            }
        ]
    ]
    ```
    #
- API Admin can add other cryptocurrency.

    METHOD : POST

    Path : http://localhost:3000/api/admin

    JSON REQ

    ```JSON
    {
        "crypto_name": "",      //  ชื่อ cryptocurrency ที่ต้องการเพิ่ม
        "rate_to_bath": "",     //  เรท cryptocurrency ที่เปลี่ยนเป็น ไทยบาท  
        "rate_to_crypto": ""    //  เรท cryptocurrency ที่เปลี่ยนจาก ไทยบาท เป็น เหรียญ 
    }
    ```
    RES status 201
    #
- API Admin manage exchange rate between cryptocurrency.

    METHOD : PUT

    Path : http://localhost:3000/api/admin/{crypto_id}

    parameter : crypto_id ที่ต้องการเปลี่ยน

    JSON REQ

    ```JSON
    {
        "rate_to_bath": "",     //  เรท cryptocurrency ที่เปลี่ยนเป็น ไทยบาท  
        "rate_to_crypto": ""    //  เรท cryptocurrency ที่เปลี่ยนจาก ไทยบาท เป็น เหรียญ 
    }
    ```
    RES status 204
    #
- API User transfer same cryptocurrency to other.

    METHOD : PUT

    Path : http://localhost:3000/api/user/same/{user_id}

    parameter : user_id ของผู้โอน

    JSON REQ

    ```JSON
    {
        "crypto_name": "",  //  ชื่อ cryptocurrency ที่ต้องการโอน
        "total": "",        //  จำนวน cryptocurrency ที่ต้องการโอน
        "user_id": ""       //  user_id ของผู้รับ
    }
    ```
    RES status 204
    #
- API User transfer cryptocurrency to other with difference currency.

    METHOD : PUT

    Path : http://localhost:3000/api/user/diff/{user_id}

    parameter : user_id ของผู้โอน

    JSON REQ

    ```JSON
    {
        "crypto_name": "",          //  ชื่อ cryptocurrency ที่ต้องการโอน
        "total": "",                //  จำนวน cryptocurrency ที่ต้องการโอน
        "diff_crypto_name": "",     //  ชื่อ cryptocurrency ที่ต้องการเทรดเป็นเหรีญอื่น
        "user_id": ""               //  user_id ของผู้รับ
    }
    ```
    RES status 204
    #

    ****หมายเหตุ : จะใช้ทศนิยม 4 ตำแหน่ง**
##
**ส่วนที่ 2  Database part**

จะอยู่ใน folder : chomchob_database_test โดยจะมี ไฟล์ sequelize.js 
ที่เขียนการสร้าง Database ในรูปแบบ sequelize และ จะมีไฟล์ ERD_DATABASE_CHOMCHOB_TEST.pdf 
เป็นไฟล์ที่วาด ERD พร้อมทั้งอธิบายรายละเอียด


ทำเติมที่ครับโปรดพิจารณาให้ผ่านด้วยครับ 😁😁




